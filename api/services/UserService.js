module.exports = {
	getUsers: function(pageNumber, next){
		var pageLimit = 5;
		User.count().exec(function(error, num){
			User.find().paginate({page: pageNumber, limit: pageLimit})
				.exec(function(err, users){
					if(err){
						throw err;
					}
					var currentPage =  parseInt(pageNumber);
					var lastPage = Math.ceil(num / pageLimit);
					_(users).forEach(function (user) {
						delete user.password
					});
					var userPageModel = {
						current_page: currentPage,
						last_page: lastPage,
						per_page: pageLimit,
						data: users
					}
					next(userPageModel);
				});
		});
	},
	getUser: function(id, next){
		User.findOne({id: id}).exec(function(error, user){
			if(err){
				throw err;
			}
			delete user.password;
			next(user);
		});
	},
	addUser: function(userModel, next){
		User.create(userModel).exec(function(err, user){
			if(err){
				throw err;
			}
			delete user.password;
			next(user);
		});
	},
	updateUser: function(userModel, next){
		User.find({id: userModel.id}).exec(function(error, user){
			if(error){
				return;
			}else{
				User.update(user[0], userModel).exec(function afterwards(err, updated){
					if(err){
						throw err;
					}
					next(updated);
				});			
			}
			
		});
	},
	removeUser: function(id, next){
		User.destroy({id: id}).exec(function(err, user){
			if(err){
				throw err;
			}
			next(user);
		});
	}
}