/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = {
	getUsers: function(req, res){
		var page = 1;
		if (req.param('page')) {
			page = req.param('page')
		}

		UserService.getUsers(page, function (userPage) {
			res.json(userPage);
		});
	},

	getUser: function(req, res){
		if (!req.param('id')) {
			res.status(400);
			return res.json();
		}

		UserService.getUser(req.param('id'), function (user) {
			res.json(user);
		});
	},

	addUser: function(req, res){
		var userModel = (req.body) ? req.body : undefined;
		UserService.addUser(userModel, function (success) {
			res.json(success);
		});
	},

	updateUser: function(req, res){
		var userModel = (req.body) ? req.body : undefined;
		UserService.updateUser(userModel, function (success) {
			res.json(success);
		});
	},

	removeUser: function(req, res){
		if (!req.param('id')) {
			res.status(400);
			return res.json();
		}

		UserService.removeUser(req.param('id'), function (success) {
			res.json(success);
		});
	}
};