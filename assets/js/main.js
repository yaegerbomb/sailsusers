const MainApp = new Vue({
	el: '#MainApp',
	store,
	components: {
		LoginComponent,
		SignupComponent,
		UserCrudComponent
	}
});