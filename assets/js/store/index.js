const state = {
	user: {

	}
}

const mutations = {
	setUser(state, user){
		state.user = user;
	}
}

const store = new Vuex.Store({
	state,
	mutations
})