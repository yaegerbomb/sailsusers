var UserCrudAddComponent = Vue.component("usercrud-add-edit-component", function(resolve, reject){
	axios.get('/js/components/account/users/crud/user.crud.add.edit.html').then(function(template){
		resolve({
			props:{
				selectedUser: {
					type: Object,
					default: {}
				}
			},
			data: function(){
				return {
					user: {
						email: "",
						password: ""
					},
					addingUser: true
				}
			},
			template: template.data,
			methods: {
				addOrUpdateUser: function(){
					if(this.addingUser){
						this.addUser();
					}else{
						this.editUser();
					}
				},
				addUserModal: function(){
					this.addingUser = true;
					$('#add-edit-user-modal').modal('show');
				},
				addUser: function(){
					eeAdd = this.emitAddEvent;
					userService.addUser(this.user)
						.then(function(response){
							if(response.status == 200){
								eeAdd();
								$('#add-edit-user-modal').modal('hide')
							}
						})
				},
				editUser: function(){
					eeEdit = this.emitAddEvent;
					userService.updateUser(this.user)
						.then(function(response){
							if(response.status == 200){
								eeEdit();
								$('#add-edit-user-modal').modal('hide')
							}
						})
				},
				cancelAction: function(){
					this.user = {};
					this.$emit('cancel-action');
				},
				emitAddEvent: function(){
					this.user = {};
					this.$emit('user-added');
				},
				emitEditEvent: function(){
					this.user = {};
					this.$emit('user-edited');
				}
			},
			watch: {
				'selectedUser': function(newVal, oldVal){
					if(this.selectedUser){
						this.addingUser = false;
						this.user = this.selectedUser;
						$('#add-edit-user-modal').modal('show');
					}
				}
			}
		});
	});
});