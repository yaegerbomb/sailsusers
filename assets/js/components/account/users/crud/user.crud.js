var UserCrudComponent = Vue.component("usercrud-component", function(resolve, reject){
	axios.get('/js/components/account/users/crud/user.crud.template.html').then(function(template){ //instead of inline templating or using webpack, using dynamic way of using components to get the template
		resolve({
			data: function(){
				return {
					users: [],
					selectedUser: {}
				}
			},
			template: template.data,
			mounted: function(){ //called on component init
				this.getUsers();
			},
			methods: {
				getUsers: function(){
					var us = this.users;
					userService.getUsers()						
						.bind(this)
						.then(function(data){
							this.users = data;						
						});
				},
				editUser: function(user){
					this.selectedUser = user;
				},
				removeUser: function(user){
					userService.removeUser(user.id)				
						.bind(this)
						.then(function(){
							this.getUsers();
						});
				},
				resetForms: function(){
					this.selectedUser = {};
				}
			}
		});
	});
});