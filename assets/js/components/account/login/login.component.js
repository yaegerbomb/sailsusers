var LoginComponent = Vue.component("login-component", function(resolve, reject){
	axios.get('/js/components/account/login/login.template.html').then(function(template){
		resolve({
			data: function(){
				return {
					user: {
						email: "",
						password: ""
					}
				}
			},
			template: template.data,
			methods: {
				login: function(){
					userService.login(this.user)
						.then(function(response){
							if(response.data.user){
								store.commit('setUser', response.data.user);								
								$('#login-modal').modal('hide')
							}
						})
				}
			}
		});
	});
});
