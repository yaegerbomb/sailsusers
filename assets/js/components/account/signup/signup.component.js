var SignupComponent = Vue.component("signup-component", function(resolve, reject){
	axios.get('/js/components/account/signup/signup.template.html').then(function(template){
		resolve({
			data: function(){
				return {
					user: {
						email: "",
						password: ""
					}
				}
			},
			template: template.data,
			methods: {
				signup: function(){
					userService.signup(this.user)
						.then(function(response){
							//go to view and close modal
							$('#signup-modal').modal('hide')
						})
				}
			}
		})
	})	
});
