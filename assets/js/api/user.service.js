const userService = {
	signup: function(user){
		return axios.post('/user', user);
	},
	login: function(user){
		return axios.post('/login', user);
	},
	getUsers: function(){
		return new Promise(function(resolve, reject){ //using blue bird to act like $q in angular. Allows us to bind 'this' in our call back so we dont have to set self ourselves. I find this cleaner
			axios.get('user/getUsers').then(function(pagedResponse){
				resolve(pagedResponse.data.data);
			});
		})
	},
	addUser: function(user){
		return axios.post('user/addUser', user);
	},
	updateUser: function(user){
		return axios.put('user/updateUser', user);
	},
	removeUser: function(id){
		return new Promise(function(resolve, reject){
			axios.delete('user/removeUser?id=' + id).then(function(data){
				resolve(data);
			});
		})
	}
}
